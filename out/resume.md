Vladimir Vujosevic
============
Email: vladimirvuj@gmail.com
Tel: +31 (0)62 557-8850


Experienced software engineer focused on problem solving that usually involves crafting performant and scallable solutions. Critical about choosing the right tool for the job and nthusiased about typed systems and functional programming, and the benefits it brings to the table. Always looking for the ways to improve the way we work and the way we build software.

## SKILLS

  - Front-end Development: JavaScript/ES6/TypeScript - (React, Vue, NextJS, Jest, Playwright, Webpack, Storybook) ReasonML/Rescript - (React) Elm HTML CSS3/SCSS/LESS 
  - Back-end Development: NodeJS (Express, NestJS) Kotlin (SpringBoot, Junit) DB (MongoDB, PostgresSQL, Redis) Kafka PHP 
  - Infra: Kubernetes - (GCP) Docker CI - (Jenkins, Github Actions, CircleCI) 

## EMPLOYMENT

### *Sr Software Engineer*, [De Bijenkorf](https://www.debijenkorf.nl) (2020-05 — Present)

De Bijenkorf is a chain of high-end department stores in the Netherlands
  - Front-end: Defining bootlenecks,improving architecture and performance of all involved projects and applications. Building and maintaining different sets of React components npm based libraries. Implementing features and AB tests, working closely with different teams and data and conversion department. Mentoring and leading junior and medior developers through consultations, code reviews, pair programming and giving technical talks.
  - Back-end: Working on API's and services, building features and maintaining services using Kotlin, Java, SpringBoot, Kafka, PostgresSQL, and Javascript and NodeJS based services. Working with CI/CD pipelines, Terraform, Helm and Kubernetes cluster.

### *Full-stack Developer*, [Weeronline and Zoover](https://www.weeronline.nl) (2016-10 — 2020-04)

Weeronline is a weather and forecasting service with more than 18 years operating on the market, currently relaunched on a modern environment
  - Owned the full development process of features on a platform that scales to thousands of users.
  - Worked accross the stuck including React SSR application, with back-end NodeJS/MongoDB service that supports the main platform
  - Helping with migration strategy of legacy platforms while planning and architecting new ideas and features for the platform.
  - Maintaining CI/CD pipelines and Kubernetes cluster.

### *Senior Front-end Developer *, [Humanity - Shiftplanning](https://www.humanity.com/) (2012-12 — 2016-07)

Humanity is a workforce management startup from San Francisco, California launched in 2009 to solve employee shift scheduling problems.
  - Worked on a Front-end SPA application based on a CanJS framework as a part of SAAS platform for workforce management.
  - Agile / Scrum process with cross functional team setup, mentoring junior team members.

### *Senior Front-end Developer*, [Computer Rock (ex. Spoiled Milk)](https://computerrock.com/) (2011-12 — 2012-12)

Computer Rock is a web agency focused on product and service design by consulting companies in business modelling, processes and product/service development
  - Building and maintaining a vast set of web applications ranging from SPA to CMS based software and hybrid mobile applications.
  - Built new features for Rails application, in-house time tracking startup project.

### *Front-end Developer*, [Complus Visual Communication](http://www.complus.rs/) (2008-07 — 2011-11)

Complus is a consulting company and web agency that offers high quality IT solutions, mostly focused on Serbian and Italian markets.
  - Building Front-end for multiple applications from small to enterprise scale based on Zend PHP framework.




## EDUCATION

### Belgrade Business School (2001-06 — 2005-01)



### XI Belgrade Gymnasium (1997-06 — 2001-01)













## INTERESTS

- SPORTS: Table tennis Wakeboarding Snowboarding Longboarding Outdoors 


