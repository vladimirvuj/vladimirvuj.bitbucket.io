Vladimir Vujosevic
============
Email: vladimirvuj@gmail.com
Tel: +31 (0)62 557-8850


Experienced software engineer with a vast history of working in the information technology industry. Before moving to Amsterdam, Netherlands in 2016, Vladimir worked for various IT companies and startups based in Belgrade, Serbia usually as a full time software developer. While his work focuses on web front-end architecture and scalability, mostly optimizing web applications for rich user experience, his non-work interests and activities range widely, everything from extreme sports to table tennis.

## SKILLS

  - Software Development: JavaScript NodeJS MongoDB HTML5 CSS3 Scala PHP 
  - Frameworks: React React Native CanJS BackboneJS 
  - CI: Travis Jenkins Circle-CI CodeCov 
  - Environments: Kubernetes Google Cloud Unix Windows 

## EMPLOYMENT

### *Senior Software Engineer*, [Weeronline and Zoover](https://www.weeronline.nl) (2016-10 — Present)

Weeronline is a dutch weather and forecasting service with more than 18 years operating on the market, currently relaunched on a modern environment, running on Kubernetes in Google Cloud™
  - Building SSR responsive React application.
  - Building and maintaining NodeJS REST API
  - Helping with a migration strategy of legacy platforms.
  - Maintaining Jenkins instances.
  - Troublshooting and maintaining Kubernetes cluster.

### *Senior Front-end Developer *, [Humanity - Shiftplanning](https://www.humanity.com/) (2012-12 — 2016-07)

Humanity is a workforce management startup from San Francisco, California launched in 2009 to solve employee shift scheduling problems.
  - Building large scale SPA JavaScript application based on a CanJS framework.
  - Agile / Scrum methodology with cross functional team setup

### *Senior Front-end Developer*, [Computer Rock (ex. Spoiled Milk)](https://computerrock.com/) (2011-12 — 2012-12)

Computer Rock is a web agency focused on product and service design by consulting companies in business modelling, processes and product/service development
  - Building and maintaining a vast set of web applications ranging from SPA to CMS based software and hybrid mobile applications.
  - Maintaining Rails application, in-house time tracking startup project.

### *Front-end Developer*, [Complus Visual Communication](http://www.complus.rs/) (2008-07 — 2011-11)

Complus is a consulting company and web agency that offers high quality IT solutions, mostly focused on Serbian and Italian markets.
  - Building Front-end for multiple applications from small to enterprise scale based on Zend PHP framework.




## EDUCATION

### Belgrade Business School (2001-06 — 2005-01)



### XI Belgrade Gymnasium (1997-06 — 2001-01)













## INTERESTS

- SPORTS: Table Tennis Snowboarding Swimming Outdoors 


